import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:signin_signup_with_bloc_and_hivedb/utils/items.dart';

class WelcomePage extends StatelessWidget {
  final pageViewController = PageController(initialPage: 0);

  WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, deviceSize: const Size(360, 690));
    return Scaffold(
      backgroundColor: const Color(0xffe0e9f8),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          Container(
            alignment: Alignment.center,
            width: ScreenUtil().setWidth(45),
            child: InkWell(
              onTap: () {},
              child: Text(
                'Skip',
                style: TextStyle(
                    color: Color(0xff347af0),
                    fontSize: ScreenUtil().setSp(15),
                    fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
      ),
      body: PageView.builder(
          physics: const NeverScrollableScrollPhysics(),
          controller: pageViewController,
          itemCount: 4,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Image.asset(
                  Items.welcomeData[index]['image']!,
                  width: ScreenUtil().setWidth(326),
                  height: ScreenUtil().setHeight(240),
                ),
                Expanded(
                  child: Container(
                    width: ScreenUtil().setWidth(375),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25),
                      ),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(16),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 25),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: List.generate(
                                4,
                                (indicator) => Container(
                                  margin: const EdgeInsets.symmetric(
                                    horizontal: 3.0,
                                  ),
                                  height: 10,
                                  width: 10,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: indicator == index
                                        ? const Color(0xff347af0)
                                        : const Color(0xffedf1f9),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Text(
                            Items.welcomeData[index]['title']!,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: ScreenUtil().setSp(30)),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            Items.welcomeData[index]['text']!,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: const Color(0xff485068),
                              fontSize: ScreenUtil().setSp(18),
                            ),
                          ),
                          Spacer(),
                          MaterialButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18),
                                side:
                                    const BorderSide(color: Color(0xff347af0))),
                            onPressed: () {
                              if (index < 3) {
                                pageViewController.animateToPage(index + 1,
                                    duration: const Duration(milliseconds: 500),
                                    curve: Curves.ease);
                              } else {}
                            },
                            color: index != 3
                                ? Colors.white
                                : const Color(0xff347af0),
                            child: Container(
                              width: ScreenUtil().setWidth(160),
                              height: 40,
                              alignment: Alignment.center,
                              child: Text(
                                index != 3
                                    ? 'Next Step'
                                    : ' Let\'s Get Started',
                                style: TextStyle(
                                  color: index != 3
                                      ? const Color(0xff347af0)
                                      : Colors.white,
                                  fontSize: ScreenUtil().setSp(16),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          }),
    );
  }
}
