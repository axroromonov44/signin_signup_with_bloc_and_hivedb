part of 'sign_in_bloc.dart';

@freezed
abstract class SignInEvent with _$SignInEvent {
  const factory SignInEvent.emailChange(String email) = EmailChange;

  const factory SignInEvent.passwordChange(String password) = PasswordChange;

  const factory SignInEvent.signInWithEmailAndPassword() =
      SignInWithEmailAndPassword;
}
