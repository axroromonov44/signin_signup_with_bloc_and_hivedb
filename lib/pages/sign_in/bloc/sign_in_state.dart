part of 'sign_in_bloc.dart';

@freezed
abstract class SignInState with _$SignInState {
  const factory SignInState({
    required String emailAddress,
    required String password,
    required bool showErrorMessage,
    required bool isSubmitting,
    required bool isLoading,
  }) = _SignInState;

  factory SignInState.initial() => SignInState(
        isLoading: false,
        emailAddress: '',
        password: '',
        showErrorMessage: false,
        isSubmitting: false,
      );
}
