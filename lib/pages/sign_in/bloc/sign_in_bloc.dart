import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';
import 'package:signin_signup_with_bloc_and_hivedb/domain/auth/i_auth_facade.dart';

part 'sign_in_event.dart';

part 'sign_in_state.dart';

part 'sign_in_bloc.freezed.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  SignInBloc(IAuthFacade iAuthFacade) : super(SignInState.initial()) {
    on<SignInEvent>(signInEvent);
  }

  Future<void> signInEvent(SignInEvent event, Emitter<SignInState> emit) async {
    await event.map(
        emailChange: (event) async {
          await state.copyWith(emailAddress: event.email);
        },
        passwordChange: (event) async {
          await state.copyWith(password: event.password);
        },
        signInWithEmailAndPassword: (event) async {});
  }
}
