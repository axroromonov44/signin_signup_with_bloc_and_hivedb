abstract class IAuthFacade {
  Future<void> signUpWithEmailAndPassword({
    required String emailAddress,
    required String password,
  });

  Future<void> sigInWithEmailAndPassword({
    required String emailAddress,
    required String password,
  });
}
