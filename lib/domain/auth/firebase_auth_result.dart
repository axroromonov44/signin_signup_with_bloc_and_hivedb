import 'package:signin_signup_with_bloc_and_hivedb/infrastructure/auth/auth_failure_or_success.dart';

class FirebaseAuthResult {
  final bool isSuccess;
  final AuthFailureOrSuccess authFailureOrSuccess;

  FirebaseAuthResult({
    required this.isSuccess,
    required this.authFailureOrSuccess,
  });
}
