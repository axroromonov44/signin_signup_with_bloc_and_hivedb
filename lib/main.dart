import 'package:flutter/material.dart';
import 'package:signin_signup_with_bloc_and_hivedb/injection.dart';
import 'package:signin_signup_with_bloc_and_hivedb/pages/sign_in/presentation/sign_in_page.dart';

void main() {
  configureDependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          elevation: 0,
          centerTitle: true,
          color: Colors.transparent,
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
        ),
      ),
      home: SignInPage(),
    );
  }
}
