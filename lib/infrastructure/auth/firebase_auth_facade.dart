import 'package:firebase_auth/firebase_auth.dart';
import 'package:signin_signup_with_bloc_and_hivedb/domain/auth/firebase_auth_result.dart';
import 'package:signin_signup_with_bloc_and_hivedb/domain/auth/i_auth_facade.dart';
import 'package:signin_signup_with_bloc_and_hivedb/infrastructure/auth/auth_failure_or_success.dart';

class FirebaseAuthFacade extends IAuthFacade {
  final FirebaseAuth _firebaseAuth;

  FirebaseAuthFacade(this._firebaseAuth);

  @override
  Future<FirebaseAuthResult> sigInWithEmailAndPassword(
      {required String emailAddress, required String password}) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
          email: emailAddress, password: password);
      return FirebaseAuthResult(
          isSuccess: true,
          authFailureOrSuccess: const AuthFailureOrSuccess.success());
    } catch (e) {
      if (e.toString() == "Error_EMAIL_ALREADY_IN_USE") {
        return FirebaseAuthResult(
            isSuccess: false,
            authFailureOrSuccess:
                const AuthFailureOrSuccess.emailAlreadyInUse());
      } else {
        return FirebaseAuthResult(
          isSuccess: false,
          authFailureOrSuccess: const AuthFailureOrSuccess.serverError(),
        );
      }
    }
  }

  @override
  Future<FirebaseAuthResult> signUpWithEmailAndPassword(
      {required String emailAddress, required String password}) async {
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
          email: emailAddress, password: password);
      return FirebaseAuthResult(
          isSuccess: true,
          authFailureOrSuccess: const AuthFailureOrSuccess.success());
    } catch (e) {
      if (e.toString() == "ERROR_INVALID_EMAIL" ||
          e.toString() == "ERROR_WRONG_PASSWORD") {
        return FirebaseAuthResult(
            isSuccess: false,
            authFailureOrSuccess:
                const AuthFailureOrSuccess.invalidEmailAndPassword());
      } else {
        return FirebaseAuthResult(
          isSuccess: false,
          authFailureOrSuccess: const AuthFailureOrSuccess.serverError(),
        );
      }
    }
  }
}
