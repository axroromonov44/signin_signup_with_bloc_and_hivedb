// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:signin_signup_with_bloc_and_hivedb/pages/sign_in/bloc/sign_in_bloc.dart';
import 'package:signin_signup_with_bloc_and_hivedb/pages/sign_up/bloc/sign_up_bloc.dart';

import 'infrastructure/auth/firebase_auth_facade.dart';
import 'domain/auth/i_auth_facade.dart';
import 'infrastructure/code/firebase_injectable_module.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String? environment,
  EnvironmentFilter? environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final firebaseInjectableModule = _$FirebaseInjectableModule();
  gh.lazySingleton<FirebaseAuth>(() => firebaseInjectableModule.firebaseAuth);
  gh.lazySingleton<IAuthFacade>(() => FirebaseAuthFacade(get<FirebaseAuth>()));
  gh.factory<SignInBloc>(() => SignInBloc(get<IAuthFacade>()));
  gh.factory<SignUpBloc>(() => SignUpBloc(get<IAuthFacade>()));
  return get;
}

class _$FirebaseInjectableModule extends FirebaseInjectableModule {}
